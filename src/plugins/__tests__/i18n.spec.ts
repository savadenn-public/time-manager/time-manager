import { i18n } from '..'

jest.mock('@/stores/DB')

describe('plugins/i18n', () => {
  it('should contains 2 languages', () => {
    expect(Object.keys(i18n.global.messages).length).toBe(2)
  })
})
