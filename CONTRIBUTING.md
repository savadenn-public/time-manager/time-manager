# Contributing

If you'd like to contribute, please read the [following document online](https://savadenn-public.gitlab.io/time-manager/time-manager/docs/) or the [sources docs/contributing.md](./docs/contributing.md)

