// eslint-disable-next-line
const StyleLintPlugin = require('stylelint-webpack-plugin');


// noinspection JSUnusedGlobalSymbols
module.exports = {
  publicPath: './',
  devServer: {
    progress: false,
  },
  configureWebpack: {
    devtool: 'source-map',
    devServer: {
      compress: true,
      progress: false,
      public: process.env.PROJECT_URL,
    },
    plugins: [
      new StyleLintPlugin({
        files: 'src/**/*.{vue,htm,html,css,sss,less,scss,sass}',
        fix: true,
      }),
      {
        apply: (compiler) =>
          compiler.hooks.done.tap('DisplayBrowserAddress', async () => {
            console.log()
            if (process.env.PROJECT_URL) {
              console.log(
                `Server running at https://${process.env.PROJECT_URL}`
              )
            }
            if (process.env.DOCS_URL) {
              console.log(`Docs running at https://${process.env.DOCS_URL}`)
            }
            console.log()
          }),
      },
    ],
  },

  chainWebpack: (config) => {
    // Inline svg
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule.use('raw-loader').loader('raw-loader')
  },

  pwa: {
    name: 'Time Manager',
    themeColor: '#006a96',
    msTileColor: '#006a96',
    iconPaths: {
      favicon32: 'img/favicon-32x32.g.png',
      favicon16: 'img/favicon-16x16.g.png',
      appleTouchIcon: 'img/logo_i_256px.g.png',
      maskIcon: 'img/logo.g.svg',
      msTileImage: 'img/logo_i_256px.g.png',
    },
  },
  productionSourceMap: false,
}
