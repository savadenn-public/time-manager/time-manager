// No dependency first
import Icon from './Icon.vue'
import Link from './Link.vue'
import Observer from './Observer.vue'

// Component with dependency
import Button from './Button.vue'
import SnackUpdate from './SnackUpdate.vue'
import StopWatch from './StopWatch.vue'

export { Button, Icon, SnackUpdate, StopWatch, Link, Observer }

export * from './controls'
