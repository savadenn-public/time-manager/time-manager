import { inject } from 'vue'

/**
 * Returns object to spread in setup of a component that use translation
 */
export function useTranslation() {
  return {
    t: inject('t', (str: unknown) => str),
  }
}
