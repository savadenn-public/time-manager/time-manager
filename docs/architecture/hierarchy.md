# Folders hierarchy

## Project {#project-tree}

| File / Folder | Usage                                            |
|:------------- |:-------------------------------------------------|
| docs          | Project documentation                            |
| src           | See `::sources-tree`                             |
| tests         | See `::tests-tree`                               |
| public        | Public files loaded at root of webserver         |

::: note
__Generated directories__

| File / Folder | Usage                                            |
|:------------- |:-------------------------------------------------|
| `.cache/`        | Yarn cache for faster docker build               |
| `coverage/`      | Code coverage reports                            |
| `dist/`          | Build output                                     |
| `node_modules/`  | JS dependencies                                  |
| `junit.xml`      | Unit test report                                 |

:::

## Sources {#sources-tree}

::: tip Source folder `./src` can be referred as `@` inside import.

Ex : `import { SnackUpdate, Footer } from '@/components'`{.js}

:::

| File / Folder          | Usage                                            |
|:---------------------- |:-------------------------------------------------|
| `src/main.ts`          | Entrypoint where Vue App is created              |
| `src/App.vue`          | Main vue App                                     |
| `src/AppError.vue`     | Application loaded if an error occur during vue initialization |
| `src/components/`      | See `::components`                                   |
| `src/helpers/`         | See `::helpers`                                      |
| `src/lang/`            | Contains translations for supported languages    |
| `src/plugins/`         | VueJS Plugins                                    |
| `src/routers/`         | Configure routing inside the app                 |
| `src/states/`          | See `::states`                                       |
| `src/style/`           | See `::style`                                        |
| `src/stores/`          | See `::stores`                                       |
| `src/types/`           | See `::types`                                        |
| `src/views/`           | See `::views`                                        |

## Tests hierarchy {#tests-tree}

| File / Folder | Usage                            |
|:------------- |:---------------------------------|
| `tests/e2e/`    | End-to-end testing with Cypress  |
| `**/__tests__/` | Unit testing with Jest           |
