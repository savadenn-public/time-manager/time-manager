/* istanbul ignore file */
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import { Home, Settings } from '@/views'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
  },
]

export const Router = createRouter({
  history: createWebHashHistory(),
  routes,
})
