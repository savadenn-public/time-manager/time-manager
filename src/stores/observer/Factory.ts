import * as Types from '@/types'
import { StoreObserver } from './StoreObserver'

type StoreObserverParams<StoredObjectType extends Types.hasId> = {
  methods: Partial<Types.StoreObserverMethods<StoredObjectType>>
}

type ObjectObserverParams<StoredObjectType extends Types.hasId> = {
  targetId: StoredObjectType['id']
  methods: Partial<Types.StoreObserverMethods<StoredObjectType>>
}

export class FactoryObservers<StoredObjectType extends Types.hasId> {
  /**
   * All observers of specific object
   * @private
   */
  private readonly objectObservers = new Map<
    StoredObjectType['id'],
    Types.StoreObservers<StoredObjectType>
  >()

  /**
   * All observers of the store
   */
  private readonly storeObservers = new Set<
    Types.StoreObserver<StoredObjectType>
  >()

  /**
   * Creates an observer for the whole store
   * @param params
   */
  createStoreObserver(params: StoreObserverParams<StoredObjectType>) {
    return new StoreObserver<StoredObjectType>({
      observers: this.storeObservers,
      methods: params.methods,
    })
  }

  /**
   * Creates on observer for a specific object of the store
   * @param params
   */
  createObjectObserver(params: ObjectObserverParams<StoredObjectType>) {
    let observers = this.objectObservers.get(params.targetId)
    if (!observers) {
      observers = new Set<StoreObserver<StoredObjectType>>()
      this.objectObservers.set(params.targetId, observers)
    }
    return new StoreObserver({ ...params, observers })
  }

  /**
   * Trigger an event on relevant store observers
   * @param event
   */
  trigger(event: Types.StoreEvent<StoredObjectType>) {
    const id  = event.action === Types.StoreAction.PUT ? event.payload.id : event.id
    const givenObjectObservers = this.objectObservers.get(id)
    if (givenObjectObservers) {
      for (const observer of givenObjectObservers) {
        observer.trigger(event)
      }
    }
    for (const observer of this.storeObservers) {
      observer.trigger(event)
    }
  }
}
