import { Entry } from '@/types'

export const testEntries: Partial<Entry>[] = [
  {
    id: 'a',
    start: new Date('2020-01-01T11:11:12'),
    stop: new Date('2020-01-01T11:12:12'),
    content: 'Content A; utf8 "←→"',
    context: 'Context client 1',
    duration: 60000,
  },
  {
    id: 'b',
    start: new Date('2020-01-02T11:11:12'),
    stop: new Date('2020-01-02T11:12:11'),
    content: 'Content B',
    context: 'Context client 2',
    duration: 61000,
  },
]
