import { deleteDB, openDB } from 'idb/with-async-ittr'
import { Schema } from '@/stores/Schema'

const DB_NAME = 'db'

async function init() {
  return openDB<Schema>(DB_NAME, 6, {
    upgrade(db, oldVersion, newVersion, transaction) {
      console.log('Upgrade DB', { oldVersion, newVersion })

      if (oldVersion < 1) {
        const entriesStore = db.createObjectStore('entries', { keyPath: 'id' })
        entriesStore.createIndex('by-start', 'start', { unique: false })
        entriesStore.createIndex('by-stop', 'stop', { unique: false })
      }

      if (oldVersion < 4) {
        const store = transaction.objectStore('entries')
        store.createIndex('by-duration', 'duration', { unique: false })
        store.getAll().then((entries) =>
          entries
            .filter((e) => !e.stop)
            .forEach((entry) => {
              entry.duration = -1
              store.put(entry)
            })
        )
      }

      if (oldVersion < 6) {
        const store = transaction.objectStore('entries')
        store.createIndex('by-context', 'context', { unique: false })
      }
    },
  })
}

let promise = init()

const reset = async function () {
  const DB = await promise
  await DB.close()
  await deleteDB(DB_NAME)
  promise = init()
}

export { promise, reset }
