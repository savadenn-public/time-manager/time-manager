file-open:/app/public/img/logo.g.svg
export-filename:/app/public/img/logo_16px.g.png
export-height:16
export-do
file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/logo_i_16px.g.png
export-height:16
export-do

file-open:/app/public/img/logo.g.svg
export-filename:/app/public/img/logo_32px.g.png
export-height:32
export-do
file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/logo_i_32px.g.png
export-height:32
export-do

file-open:/app/public/img/logo.g.svg
export-filename:/app/public/img/logo_256px.g.png
export-height:256
export-do
file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/logo_i_256px.g.png
export-height:256
export-do


file-open:/app/public/img/logo.g.svg
export-filename:/app/public/img/logo_512px.g.png
export-height:512
export-do
file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/logo_i_512px.g.png
export-height:512
export-do


file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/favicon-16x16.g.png
export-height:16
export-background: #006a96
export-background-opacity:1
export-do

file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/favicon-32x32.g.png
export-height:32
export-background: #006a96
export-background-opacity:1
export-do

file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/icons/android-chrome-192x192.png
export-height:192
export-background: #006a96
export-background-opacity:1
export-do

file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/icons/android-chrome-512x512.png
export-height:512
export-background: #006a96
export-background-opacity:1
export-do

file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/icons/android-chrome-maskable-192x192.png
export-height:192
export-background: #006a96
export-background-opacity:1
export-do

file-open:/app/public/img/logo.i.g.svg
export-filename:/app/public/img/icons/android-chrome-maskable-512x512.png
export-height:512
export-background: #006a96
export-background-opacity:1
export-do
