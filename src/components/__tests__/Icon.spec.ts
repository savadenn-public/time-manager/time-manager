import { shallowMount } from '@vue/test-utils'
import { Icon } from '..'

describe('Components/Icon.vue', () => {
  it('renders icon', async () => {
    const viewBox = '0 0 32 32'
    const icon = '12 23 45 56'
    const wrapper = shallowMount(Icon, { props: { viewBox, icon } })
    const svg = wrapper.find('svg')
    expect(svg.exists()).toBe(true)
    expect(svg.html()).toEqual(
      `<svg viewBox="${viewBox}"><path d="${icon}"></path></svg>`
    )
  })
})
