import { computed } from 'vue'
import { SetupContext } from '@vue/runtime-core'
import { ControlEmits } from '@/components/controls/mixins/IsControl'

export const ModelEventUpdate = 'update:modelValue'
export const ModelEmits = [ModelEventUpdate]

/**
 * Delays between user input and firing change of value
 */
export const CONTROL_DEBOUNCE_DELAY = 300

export interface ModelProps<ValueType> {
  modelValue?: ValueType | undefined

  /**
   * Debounce delay in milliseconds
   * if not set, default is 300
   */
  debounceDelay?: number
}

export interface HandlesModelParams<ValueType> {
  props: ModelProps<ValueType>
  context: SetupContext<typeof ControlEmits>

  /**
   * return true if value if valid
   * @param value
   */
  validate?: (value: unknown) => boolean

  /**
   * Process value after debounce and before triggering event
   * @param value
   */
  processValue?: (value: ValueType | undefined) => ValueType | undefined

  /**
   * Process modelValue (from props) before injecting it to value
   * @param value
   */
  processModelValue?: (value: ValueType | undefined) => ValueType | undefined
}

/**
 * Returns object to spread in setup of a component that can handle v-model
 * @param params
 */
export function handlesModel<ValueType>(params: HandlesModelParams<ValueType>) {
  let valueTimeout = 0

  const isValid = computed(
    () => !params.validate || params.validate(params.props.modelValue)
  )
  return {
    /**
     * True if valid, or if no validation method is given
     */
    isValid,
    value: computed({
      get: () =>
        params.processModelValue
          ? params.processModelValue(params.props.modelValue)
          : params.props.modelValue,
      set: (value: ValueType | undefined) => {
        if (valueTimeout) {
          clearTimeout(valueTimeout)
        }
        valueTimeout = window.setTimeout(() => {
          if (!isValid.value) return
          params.context.emit(
            ModelEventUpdate,
            params.processValue ? params.processValue(value) : value
          )
        }, params.props.debounceDelay ?? CONTROL_DEBOUNCE_DELAY)
      },
    }),
  }
}
