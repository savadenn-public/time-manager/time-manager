import * as Types from '.'

export interface StoreObserverMethods<StoredObjectType extends Types.hasId> {
  /**
   * Method executed when PUT event is detected
   * @param event
   */
  onPut?: (event: Types.PutStoreEvent<StoredObjectType>) => void | Promise<void>

  /**
   * Method executed when DELETE event is detected
   * @param event
   */
  onDelete?: (
    event: Types.DeleteStoreEvent<StoredObjectType>
  ) => void | Promise<void>
}

export interface StoreObserver<StoredObjectType extends Types.hasId>
  extends StoreObserverMethods<StoredObjectType> {
  /**
   * Trigger given event
   * @param event
   */
  trigger: (event: Types.StoreEvent<StoredObjectType>) => void | Promise<void>

  /**
   * Start observation
   */
  start(): void

  /**
   * Stop observation
   */
  stop(): void
}

/**
 * Set of observers
 */
export type StoreObservers<StoredObjectType extends Types.hasId> = Set<
  StoreObserver<StoredObjectType>
>

/**
 * Sets of observer aliased by id
 */
export type ObjectObservers<StoredObjectType extends Types.hasId> = Map<
  StoredObjectType['id'],
  StoreObservers<StoredObjectType>
>
