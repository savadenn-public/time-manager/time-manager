import { expectMethodsCallTimes, getTestingSample, testId } from './helpers'
import { StoreAction } from '@/types'

describe(__dirname + '/observer.vue', () => {
  it('creates a store observer and object observer', async () => {
    const sample = getTestingSample()
    expect(sample.storeObserver).not.toBeUndefined()
    expect(sample.objectObserver).not.toBeUndefined()
  })

  it('works when observer has no methods', async () => {
    const sample = getTestingSample()
    delete sample.storeObserver.onPut
    delete sample.storeObserver.onDelete
    delete sample.objectObserver.onPut
    delete sample.objectObserver.onDelete
    sample.storeObserver.start()
    sample.objectObserver.start()

    const payload = { id: testId }

    sample.factory.trigger({
      id: payload.id,
      action: StoreAction.DELETE,
    })
    sample.factory.trigger({
      payload,
      action: StoreAction.PUT,
    })
  })

  it('triggers storeObserver on event on store', async () => {
    const sample = getTestingSample()
    const payload = { id: 'not' + testId }
    sample.storeObserver.start()
    sample.objectObserver.start()

    sample.factory.trigger({ payload, action: StoreAction.PUT })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 0 },
        store: { onDelete: 0, onPut: 1 },
      },
    })

    sample.factory.trigger({
      id: payload.id,
      action: StoreAction.DELETE,
    })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 0 },
        store: { onDelete: 1, onPut: 1 },
      },
    })
  })

  it('triggers storeObserver and objectObserver on event on store', async () => {
    const sample = getTestingSample()
    const payload = { id: testId }
    sample.storeObserver.start()
    sample.objectObserver.start()

    sample.factory.trigger({ payload, action: StoreAction.PUT })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 1 },
        store: { onDelete: 0, onPut: 1 },
      },
    })

    sample.factory.trigger({
      id: payload.id,
      action: StoreAction.DELETE,
    })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 1, onPut: 1 },
        store: { onDelete: 1, onPut: 1 },
      },
    })
  })

  it('does not triggers storeObserver and objectObserver when not started', async () => {
    const sample = getTestingSample()
    const payload = { id: testId }

    sample.storeObserver.start()
    sample.objectObserver.start()
    sample.storeObserver.stop()
    sample.objectObserver.stop()

    sample.factory.trigger({ payload, action: StoreAction.PUT })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 0 },
        store: { onDelete: 0, onPut: 0 },
      },
    })

    sample.factory.trigger({
      id: payload.id,
      action: StoreAction.DELETE,
    })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 0 },
        store: { onDelete: 0, onPut: 0 },
      },
    })
  })

  it('does not triggers storeObserver and objectObserver when stopped', async () => {
    const sample = getTestingSample()
    const payload = { id: testId }

    sample.factory.trigger({ payload, action: StoreAction.PUT })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 0 },
        store: { onDelete: 0, onPut: 0 },
      },
    })

    sample.factory.trigger({
      id: payload.id,
      action: StoreAction.DELETE,
    })
    expectMethodsCallTimes({
      sample,
      expected: {
        object: { onDelete: 0, onPut: 0 },
        store: { onDelete: 0, onPut: 0 },
      },
    })
  })
})
