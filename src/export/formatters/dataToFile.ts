/* istanbul ignore file */
/* File API is not supported by Jest engine (JSDOM), this is mocked in test */

/**
 * Export function for mocking in test
 * @param fileBits
 * @param fileName
 * @param options
 */
export function dataToFile(
  fileBits: BlobPart[],
  fileName: string,
  options?: FilePropertyBag
) {
  return new File(fileBits, fileName, options)
}
