export const message = {
  _name: 'English',
  download: 'Download entries',
  pwa: {
    available: 'Update is available',
    update: 'Update now',
  },
  home: {
    view: 'Home',
  },
  entries: {
    running: 'Running entries',
    ended: 'Ended entries',
  },
  navbar: {
    toggle: 'Toggle menu',
  },
  settings: {
    title: 'Settings',
    language: 'Language',
    timeZone: 'Time Zone',
    reset: 'Use default settings',
    theme: 'Theme',
    sameAsBrowser: 'Use browser settings',
    themeLight: 'Light',
    themeDark: 'Dark',
    restore: 'Restore previous settings',
    downloadFormat: 'Export format',
    stopOnStart: 'Auto stop running entries when starting a new one',
    groupBy: {
      month: 'Group entries by months',
      week: 'Group entries by weeks',
      day: 'Group entries by days',
    },
    recompute: 'Normalize entries',
  },
  component: {
    restore: 'Cancel change',
  },
  entry: {
    content: 'Description',
    context: 'Context',
    new: 'New time entry',
    remove: 'Remove',
    stop: 'Stop',
    off: 'Away',
    edit: 'Edit entry',
    save: 'Save',
    date: {
      start: 'Start date & time',
      stop: 'End date & time',
    },
  },
}

export type Translations = typeof message

export const en = message as unknown as { [name: string]: string }
