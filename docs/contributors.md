# Authors and contributors {#contributors}

Time Manager is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found [here](https://gitlab.com/savadenn-public/time-manager/time-manager/-/graphs/stable).
