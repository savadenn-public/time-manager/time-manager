import { dataToFile } from '../dataToFile'
import { testEntries } from './testEntries'
import { saveAs } from 'file-saver'
import { csv, json, yaml } from '..'
import { mocked } from 'ts-jest/utils'
import Fs from 'fs/promises'
import Path from 'path'
import { Formatter } from '@/export/formatters/Formatter'
import ProvidesCallback = jest.ProvidesCallback

jest.mock('file-saver', () => ({ saveAs: jest.fn() }))
jest.mock('../dataToFile', () => ({ dataToFile: jest.fn() }))

const mockedDataToFile = mocked(dataToFile, true)
const mockedSaveAs = mocked(saveAs, true)

beforeEach(() => {
  mockedDataToFile.mockReset()
  mockedSaveAs.mockReset()
})

/**
 * Call download method of given formatter and test it
 * @param formatter
 * @param trimExpected
 */
function testDownload(
  formatter: Formatter,
  trimExpected = false
): ProvidesCallback {
  return async () => {
    await formatter.download(testEntries)

    expect(mockedDataToFile).toHaveBeenCalledTimes(1)

    const convertDataParameters = mockedDataToFile.mock.calls[0]
    expect(convertDataParameters[1]).toEqual(formatter.filename)
    expect(convertDataParameters[2]).toEqual({ type: formatter.contentType })

    const expectedContent = await Fs.readFile(
      Path.join(__dirname, formatter.filename),
      { encoding: 'utf-8' }
    )
    const expected = trimExpected ? expectedContent.trimEnd() : expectedContent
    expect(convertDataParameters[0]).toEqual([expected])
  }
}

/**
 * Formatters test suite
 */
describe('export/Download', () => {
  it('exports entries as csv', testDownload(csv))
  it('exports entries as json', testDownload(json, true))
  it('exports entries as yaml', testDownload(yaml))
})
