/**
 * Action available when crash at start
 */
export interface Action {
  /**
   * Name of action
   */
  name: string

  /**
   * User description
   */
  label: string

  /**
   * Action handler
   */
  run: Function
}
