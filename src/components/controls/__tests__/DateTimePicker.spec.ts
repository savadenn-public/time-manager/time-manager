import { mount } from '@vue/test-utils'
import { DateTimePicker } from '..'
import moment from 'moment-timezone'
import {
  getUpdateModelEvent,
  waitDebounce,
} from '@/components/controls/__tests__/Control.helper'

/**
 * Help method to test DateTimePicker component
 * @param params
 */
function testDateTimePicker(params: {
  timeZone: string
  mode: 'time' | 'date'
  modelValue: Date
  inputString: string
  expectedDate: Date
}) {
  it(`change model when ${params.mode} is entered (${
    params.timeZone
  } timezone, DST: ${moment(params.modelValue).isDST()})`, async () => {
    const wrapper = mount(DateTimePicker, {
      props: {
        modelValue: params.modelValue,
        mode: params.mode,
        locale: 'fr-FR',
        timeZone: params.timeZone,
        debounceDelay: 10,
      },
    })
    const input = wrapper.get('.DateTimePicker__Input')
    await input.setValue(params.inputString)
    await waitDebounce(10)
    const eventEmitted = getUpdateModelEvent(wrapper)
    expect(eventEmitted.length).toBe(1)
    expect(eventEmitted[0]).toEqual([params.expectedDate])
  })
}

describe(__dirname + __filename, () => {
  const timezones = [
    'Pacific/Kiritimati', // +14,
    'Europe/London', // +1
    'Europe/Paris', // +2
    'Pacific/Rarotonga', // -10
  ]
  const modelValues = [
    moment('2021-06-03T08:34:56Z'), // Summer time
    moment('2021-01-03T08:34:56Z'), // Winter time
  ]
  for (const modelValue of modelValues) {
    for (const timeZone of timezones) {
      testDateTimePicker({
        timeZone,
        mode: 'time',
        modelValue: modelValue.clone().toDate(),
        inputString: '18:42',
        expectedDate: modelValue
          .clone()
          .tz(timeZone)
          .hour(18)
          .minute(42)
          .second(0)
          .toDate(),
      })

      testDateTimePicker({
        timeZone,
        mode: 'date',
        modelValue: modelValue.clone().toDate(),
        inputString: '20/12/2020',
        expectedDate: modelValue
          .clone()
          .tz(timeZone)
          .year(2020)
          .month(11) // Month is 0 - 11 based
          .date(20)
          .toDate(),
      })
    }
  }
})
