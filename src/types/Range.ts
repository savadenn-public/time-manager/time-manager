/**
 * A time range
 */
export interface Range {
  /**
   * Start date
   */
  start?: Date

  /**
   * Stop date
   */
  stop?: Date
}
