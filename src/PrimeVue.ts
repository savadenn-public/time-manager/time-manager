import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'

import PrimeVue from 'primevue/config'
import { createApp } from 'vue'
import Chart from 'primevue/chart'
import InputText from 'primevue/inputtext'
import Editor from 'primevue/editor'
import Inplace from 'primevue/inplace'
import Button from 'primevue/button'
import AutoComplete from 'primevue/autocomplete'
import ScrollTop from 'primevue/scrolltop'
import ProgressSpinner from 'primevue/progressspinner'
import Skeleton from 'primevue/skeleton'

/**
 * Register all needed PrimeVue Component
 */
export function registerPrimeVue(app: ReturnType<typeof createApp>): void {
  // Load PrimeVue
  app.use(PrimeVue, { ripple: true, inputStyle: 'filled' })

  // Register PrimeVue Component
  app.component('AutoComplete', AutoComplete)
  app.component('Button', Button)
  app.component('Chart', Chart)
  app.component('Editor', Editor)
  app.component('Inplace', Inplace)
  app.component('InputText', InputText)
  app.component('ProgressSpinner', ProgressSpinner)
  app.component('ScrollTop', ScrollTop)
  app.component('Skeleton', Skeleton)
}
