/**
 * Quick fix
 * TODO check https://github.com/nathanreyes/v-calendar/issues/827
 */

declare module 'v-calendar' {
  export const DatePicker:
    | Exclude<Plugin['install'], undefined>
    | DefineComponent
}
