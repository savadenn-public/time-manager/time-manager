export type Theme = 'themeLight' | 'themeDark'
export type DownloadFormat = 'yaml' | 'json' | 'csv'
