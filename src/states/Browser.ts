import moment from 'moment-timezone'

/**
 * Get the timeZone of the browser
 */
export function getTimeZone() {
  return moment.tz.guess(true)
}

/**
 * Get lang of the browser
 */
export function getLang() {
  return window.navigator.language.substr(0, 2)
}

/**
 * Get theme of browser
 */
export function getTheme() {
  return matchMedia('(prefers-color-scheme: dark)').matches
    ? 'themeDark'
    : 'themeLight'
}
