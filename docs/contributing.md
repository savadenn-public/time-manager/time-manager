# Contributing {#contributing}

The project is managed on [GitLab](https://gitlab.com/savadenn-public/time-manager/time-manager).

When contributing to this repository, please first discuss the change you wish to make :
* via an [issue](https://gitlab.com/savadenn-public/time-manager/time-manager/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
* via our public [Matrix room](https://matrix.to/#/#time-manager:savadenn.ems.host)


## Standard commit message

Commits **must** follow the [Conventional commits rules](https://www.conventionalcommits.org)

::: tip
There are plugins for your IDE. Ex: [Jetbrains plugin](https://plugins.jetbrains.com/plugin/13389-conventional-commit), [VSCode plugin](https://marketplace.visualstudio.com/items?itemName=vivaxy.vscode-conventional-commits)
:::

| Type     | Description |
|:-------- | :-------------------------------------------------------------------------------------------------------- |
| refactor | Changes which neither fix a bug nor add a feature |
| fix      | Changes which patch a bug |
| feat     | Changes which introduce a new feature |
| build    | Changes which affect the build system or external dependencies.<br/>Example scopes: gulp, broccoli, npm |
| chore    | Changes which aren't user-facing |
| style    | Changes which don't affect code logic, such as white-spaces, formatting, missing semi-colons |
| test     | Changes which add missing tests or correct existing tests |
| docs     | Changes which affect documentation |
| perf     | Changes which improve performance |
| ci       | Changes which affect CI configuration files and scripts.<br/>Example scopes: travis, circle, browser-stack, sauce-labs |
| revert   | Changes which revert a previous commit |

| Footer          | Description |
|:-------- |:--------------------------------------------------------------------------------------------------------- |
| BREAKING CHANGE | The commit introduces breaking API changes |
| Closes          | The commit closes issues or merge requests |
| See             | The commit references issues or merge requests |
| Implements      | The commit implements features |
| Co-authored-by  | The commit is co-authored by another person.<br/>For multiple people use one line each |
| Refs            | The commit references other commits by their hash ID.<br/>For multiple hash IDs use a comma as separator |

