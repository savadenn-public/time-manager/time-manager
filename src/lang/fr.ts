import { Translations } from '@/lang'

const message: Translations = {
  _name: 'Français',
  download: 'Télécharger les entrées',
  pwa: {
    available: 'Une mise à jour est disponible',
    update: 'Mettre à jour',
  },
  home: {
    view: 'Page principale',
  },
  entries: {
    running: 'Entrées en cours',
    ended: 'Entrées terminées',
  },
  navbar: {
    toggle: 'Afficher le menu',
  },
  settings: {
    title: 'Paramètres',
    language: 'Langue',
    timeZone: 'Fuseau horaire',
    reset: 'Utiliser la configuration par défaut',
    theme: 'Thème',
    sameAsBrowser: 'Utiliser la configuration navigateur',
    restore: 'Annuler toutes les modifications',
    themeLight: 'Clair',
    themeDark: 'Sombre',
    downloadFormat: 'Format d’export',
    stopOnStart:
      'Arrêt automatique des tâches lors de la création d’une nouvelle',
    groupBy: {
      month: 'Grouper les entrées par mois',
      week: 'Grouper les entrées par semaine',
      day: 'Grouper les entrées par jour',
    },
    recompute: 'Réparer les entrées',
  },
  component: {
    restore: 'Annuler la modification',
  },
  entry: {
    content: 'Description',
    context: 'Contexte',
    new: 'Nouvelle entrée',
    remove: 'Supprimer cette entrée',
    stop: 'Arrêter',
    off: 'Absent',
    edit: 'Modifier cet entrée',
    save: 'Enregistrer',
    date: {
      start: 'Date & heure de début',
      stop: 'Date & heure de fin',
    },
  },
}

export const fr = message as unknown as { [name: string]: string }
