export * as Browser from './Browser'
import { startTime } from '@/states/Now'
import { loadSavedSettings } from '@/states/Settings'
export * from './PwaUpdate'
export * from './Settings'
export * from './Now'

/**
 * Initialize states asynchronously
 */
export async function initialize() {
  return Promise.all([startTime(), loadSavedSettings()])
}
