import { CONTROL_DEBOUNCE_DELAY } from '@/components/controls/mixins'
import { nextTick } from 'vue'

export async function waitDebounce(delay: number = CONTROL_DEBOUNCE_DELAY) {
  await new Promise((resolve) => {
    setTimeout(resolve, delay)
  })
  await nextTick()
}

export function getUpdateModelEvent<ValueType>(wrapper: {
  emitted(): Record<string, ValueType[]>
}) {
  expect(wrapper.emitted()).toHaveProperty('update:modelValue')
  return wrapper.emitted()['update:modelValue']
}
