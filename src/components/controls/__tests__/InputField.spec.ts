import { shallowMount } from '@vue/test-utils'
import { InputField } from '..'
import {
  getUpdateModelEvent,
  waitDebounce,
} from '@/components/controls/__tests__/Control.helper'

const label = 'My field'
const modelValue = 'initValue'

describe('/InputField.vue', () => {
  it('renders label when passed', async () => {
    const wrapper = shallowMount(InputField, { props: { label, modelValue } })
    expect(wrapper.text()).toMatch(label)
  })

  it('change value when typed after debounce', async () => {
    const wrapper = shallowMount(InputField, {
      props: { label, modelValue },
    })
    const control = wrapper.find('.InputField__Control')

    // Changing twice to test debounce
    await control.setValue('myValueFirst')
    await control.setValue('myValueSecond')

    await waitDebounce()
    const eventEmitted = getUpdateModelEvent(wrapper)

    // If length is 2, it's means that the debounce in ../mixins/HandlesModel.ts failed
    expect(eventEmitted.length).toBe(1)
    expect(eventEmitted[0]).toEqual(['myValueSecond'])
  })

  it('does not signal change without restoreValue', async () => {
    const wrapper = shallowMount(InputField, {
      props: { label, modelValue },
    })
    expect(wrapper.vm.hasChanged).toEqual(false)
    const control = wrapper.find('.InputField__Control')
    await control.setValue('myValueFirst')
    await waitDebounce()
    expect(wrapper.vm.hasChanged).toEqual(false)
  })

  it('signals change with restoreValue', async () => {
    const wrapper = shallowMount(InputField, {
      props: { label, modelValue, restoreValue: modelValue },
    })
    expect(wrapper.vm.hasChanged).toEqual(false)
    const control = wrapper.find('.InputField__Control')
    await control.setValue('myValueFirst')
    await waitDebounce()
    expect(wrapper.vm.hasChanged).toEqual(true)
  })
})
