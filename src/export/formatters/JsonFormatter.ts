import { Formatter } from './internal'
import { Entry } from '@/types'

export class JsonFormatter extends Formatter {
  contentType = 'application/json;charset=utf-8'
  filename = 'entries.json'

  formatSpecific(entries: Entry[]) {
    return JSON.stringify(entries, null, 2)
  }
}

/**
 * Singleton instance of JsonFormatter
 */
export const jsonFormatter = new JsonFormatter()
