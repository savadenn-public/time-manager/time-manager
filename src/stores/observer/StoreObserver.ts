import * as Types from '@/types'

type StoreObserverParams<StoredObjectType extends Types.hasId> = {
  observers: Types.StoreObservers<StoredObjectType>
  methods: Partial<Types.StoreObserverMethods<StoredObjectType>>
}

/**
 * Observer for a whole store
 */
export class StoreObserver<StoredObjectType extends Types.hasId>
  implements Types.StoreObserver<StoredObjectType> {
  private readonly observers: Types.StoreObservers<StoredObjectType>

  onPut?: (event: Types.PutStoreEvent<StoredObjectType>) => void | Promise<void>
  onDelete?: (event: Types.DeleteStoreEvent<StoredObjectType>) => void | Promise<void>

  constructor(params: StoreObserverParams<StoredObjectType>) {
    this.onDelete = params.methods.onDelete
    this.onPut = params.methods.onPut
    this.observers = params.observers
  }

  start() {
    this.observers.add(this)
  }

  stop() {
    this.observers.delete(this)
  }

  trigger(event: Types.StoreEvent<StoredObjectType>) {
    switch (event.action) {
      case Types.StoreAction.DELETE:
        return this.onDelete && this.onDelete(event)
      case Types.StoreAction.PUT:
        return this.onPut && this.onPut(event)
    }
  }
}
