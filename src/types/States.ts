import { Theme, DownloadFormat } from './internal'
import * as Types from './internal'

export interface PwaUpdate {
  refreshing: boolean
  registration: ServiceWorkerRegistration | null
  updateExists: boolean

  read(): void
}

export const SAME_AS_BROWSER = 'settings.sameAsBrowser'

export interface Settings {
  lang: string
  timeZone: string
  theme: Theme | typeof SAME_AS_BROWSER
  downloadFormat: DownloadFormat
  stopOnStart: boolean
  groupBy: {
    month: boolean
    week: boolean
    day: boolean
  }
}

/**
 * App state for entry
 */
export interface Entry {
  /**
   * Timestamp of latest changes, used to trigger rerender
   */
  changes: Record<Types.Entry['id'], number>
}
