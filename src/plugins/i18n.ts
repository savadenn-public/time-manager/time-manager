import { createI18n } from 'vue-i18n'
import { en, fr } from '@/lang'
import * as moment from 'moment-timezone'
import 'moment/locale/fr'
import 'moment/locale/en-gb'
import { locale } from '@/states'
import { watchEffect } from 'vue'

export const i18n = createI18n({
  fallbackLocale: 'en',
  locale: locale.value,
  messages: { en, fr },
  silentFallbackWarn: true,
})

const CODES: Record<string, string> = {
  en: 'en-gb',
  fr: 'fr',
}

watchEffect(() => {
  i18n.global.locale = locale.value
  moment.locale(CODES[locale.value])
})
