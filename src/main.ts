/* istanbul ignore file */
import { createApp } from 'vue'
import App from '@/App.vue'
import AppError from '@/AppError.vue'
import { Router } from '@/router'
import * as Plugins from '@/plugins'
import '@/registerServiceWorker'
import '@/style/main.scss'
import { initialize } from '@/states'
import { registerPrimeVue } from '@/PrimeVue'

// Styles
require('typeface-montserrat')
import 'primeicons/primeicons.css'
import 'primevue/resources/primevue.min.css'

initialize().catch((e) => console.error('Error at initialize', { e }))

try {
  const app = createApp(App)
  app.use(Router)
  registerPrimeVue(app)
  app.use(Plugins.i18n)
  app.mount('body')
} catch (error) {
  createApp(AppError, { error }).mount('body')
}
