import { Entry, Formatter as Interface } from '@/types'
import { saveAs } from 'file-saver'
import { dataToFile } from '@/export/formatters/dataToFile'
import * as Types from '@/types'

/**
 * Format and download Entries
 */
export abstract class Formatter implements Interface {
  abstract contentType: string
  abstract filename: string

  format(entries: Partial<Entry>[]): string {
    return this.formatSpecific(
      entries.map((entry: Partial<Types.Entry>) => {
        return {
          id: entry.id,
          content: entry.content,
          context: entry.context,
          duration: entry.duration,
          start: entry.start,
          stop: entry.stop,
        }
      })
    )
  }

  /**
   * Format specifically for this format
   * @param entries
   */
  abstract formatSpecific(entries: Partial<Entry>[]): string

  /**
   * Generate data and trigger browser download
   */
  async download(entries: Partial<Entry>[]) {
    const data = this.format(entries)
    saveAs(dataToFile([data], this.filename, { type: this.contentType }))
  }
}
