import { shallowMount } from '@vue/test-utils'
import { SplitDateTimePicker } from '..'
import moment from 'moment-timezone'

const baseMoment = moment('2021-04-03T12:34:56Z')
const modelValue = baseMoment.toDate()

describe(__dirname + '/SplitDateTimePicker.vue', () => {
  it('display date picker on date button click', async () => {
    const wrapper = shallowMount(SplitDateTimePicker, {
      props: { modelValue, foldTime: true, foldDate: true },
    })
    const dateButton = wrapper.find('.SplitDateTimePicker__dateButton')
    const datePart = wrapper.find('.SplitDateTimePicker__date')
    expect(datePart.classes()).toContain('SplitDateTimePicker--folded')
    await dateButton.trigger('click')
    expect(datePart.classes()).not.toContain('SplitDateTimePicker--folded')
  })

  it('display time picker on time button click', async () => {
    const wrapper = shallowMount(SplitDateTimePicker, {
      props: { modelValue, foldDate: true, foldTime: true },
    })
    const dateButton = wrapper.find('.SplitDateTimePicker__timeButton')
    const timePart = wrapper.find('.SplitDateTimePicker__time')
    expect(timePart.classes()).toContain('SplitDateTimePicker--folded')
    await dateButton.trigger('click')
    expect(timePart.classes()).not.toContain('SplitDateTimePicker--folded')
  })
})
