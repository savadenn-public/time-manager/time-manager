import { shallowMount } from '@vue/test-utils'
import { StopWatch } from '..'
import moment from 'moment-timezone'

describe(__dirname + __filename, () => {
  it('displays precise time', async () => {
    const props = {
      format: 'HH:mm',
      now: moment('2021-04-01T14:25:00+01:00'),
      start: moment('2021-04-01T12:20:00Z'),
    }
    const wrapper = shallowMount(StopWatch, { props: { ...props } })
    const precise = wrapper.find('.StopWatch--precise')
    expect(precise.text()).toEqual('an hour 5 minutes')
  })
})
