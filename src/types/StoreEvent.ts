export const enum StoreAction {
  PUT,
  DELETE,
}

export type hasId = { id: unknown }

export interface PutStoreEvent<StoredObjectType extends hasId> {
  action: StoreAction.PUT
  payload: StoredObjectType
}

export interface DeleteStoreEvent<StoredObjectType extends hasId> {
  action: StoreAction.DELETE
  id: StoredObjectType['id']
}

export type StoreEvent<StoredObjectType extends hasId> =
  | DeleteStoreEvent<StoredObjectType>
  | PutStoreEvent<StoredObjectType>
