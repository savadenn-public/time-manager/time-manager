# Technical stack

This project uses :

* [Vue 3](https://v3.vuejs.org/) as framework and was set with [Vue cli 4](https://cli.vuejs.org)
  * Build uses Webpack + Babel
* Sources are written with [Typescript](https://www.typescriptlang.org/)
* Linting uses :
  * [ESLint](https://eslint.org/)
  * [Prettier](https://prettier.io/)
  * [Stylelint](https://stylelint.io/)
* Unit testing :
  * [Jest](https://jestjs.io/)
  * [Vue Test Utils](https://next.vue-test-utils.vuejs.org/)
* E2E testing : [Cypress](https://www.cypress.io/)
* The whole dev stack use docker & docker-compose
* Persistence
  * Local persistance uses browser database [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API) with the library [idb](https://github.com/jakearchibald/idb) for a promise approach.
