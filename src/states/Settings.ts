import { computed, reactive, watchEffect } from 'vue'
import { States, Theme } from '@/types'
import { Browser } from '.'

export const DEFAULT_SETTINGS: States.Settings = {
  lang: States.SAME_AS_BROWSER,
  theme: States.SAME_AS_BROWSER,
  timeZone: States.SAME_AS_BROWSER,
  downloadFormat: 'yaml',
  stopOnStart: true,
  groupBy: {
    month: true,
    week: true,
    day: true,
  },
}

export const settings: States.Settings = reactive({ ...DEFAULT_SETTINGS })

/**
 * Current locale value
 */
export const locale = computed(() =>
  settings.lang === States.SAME_AS_BROWSER ? Browser.getLang() : settings.lang
)

/**
 * Current theme
 */
export const theme = computed<Theme>(() => {
  return settings.theme === States.SAME_AS_BROWSER
    ? Browser.getTheme()
    : (settings.theme as Theme)
})

/**
 * Current timeZone
 */
export const timeZone = computed<string>(() => {
  return settings.timeZone === States.SAME_AS_BROWSER
    ? Browser.getTimeZone()
    : settings.timeZone
})

// Load saved settings
const SETTINGS_KEY = 'settings'

export async function loadSavedSettings() {
  const savedSettings = localStorage.getItem(SETTINGS_KEY)
  if (savedSettings) {
    Object.assign(settings, JSON.parse(savedSettings))
  }

  // On change save settings
  watchEffect(() =>
    localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings))
  )
}
