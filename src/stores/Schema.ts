import { DBSchema } from 'idb/with-async-ittr'
import * as Types from '@/types'

export interface Schema extends DBSchema {
  entries: {
    value: Types.Entry
    key: Types.Entry['id']
    indexes: {
      'by-start': Types.Entry['start']
      'by-stop': Types.Entry['start'] // Stop is Date | undefined and cannot be used as indexes types
      'by-duration': Types.Entry['duration']
      'by-context': string
    }
  }

  logs: {
    value: {
      content: string
    }
    key: string
  }
}
