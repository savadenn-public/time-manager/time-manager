# Components

_Located in `./src/components`_

This module contains components used in `::views`.

## Rules for writing components

* Respect [single-file components](https://v3.vuejs.org/guide/single-file-component.html#single-file-components) structure
* Pass everything with dependency injection via props
  * Allow easier reuse of components as well as unit testing

::: note
`::types` import are allowed
:::
