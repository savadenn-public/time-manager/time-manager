import { shallowMount } from '@vue/test-utils'
import { ToggleButton } from '..'
import {
  getUpdateModelEvent,
  waitDebounce,
} from '@/components/controls/__tests__/Control.helper'

const label = 'My boolean'
const modelValue = false

describe(__dirname + '/ToggleButton.vue', () => {
  it('renders label when passed', async () => {
    const wrapper = shallowMount(ToggleButton, { props: { label, modelValue } })
    expect(wrapper.text()).toMatch(label)
  })

  it('change value when clicked', async () => {
    const wrapper = shallowMount(ToggleButton, {
      props: { label, modelValue },
    })
    const control = wrapper.find('.ToggleButton__Control')
    await control.trigger('click')
    await waitDebounce()
    const eventEmitted = getUpdateModelEvent(wrapper)
    expect(eventEmitted.length).toBe(1)
    expect(eventEmitted[0]).toEqual([true])
  })

  it('does not signal change without restoreValue', async () => {
    const wrapper = shallowMount(ToggleButton, {
      props: { label, modelValue },
    })
    expect(wrapper.vm.hasChanged).toEqual(false)
    const control = wrapper.find('.ToggleButton__Control')
    await control.setValue(true)
    await waitDebounce()
    expect(wrapper.vm.hasChanged).toEqual(false)
  })

  it('signals change with restoreValue', async () => {
    const wrapper = shallowMount(ToggleButton, {
      props: { label, modelValue, restoreValue: modelValue },
    })
    expect(wrapper.vm.hasChanged).toEqual(false)
    const control = wrapper.find('.ToggleButton__Control')
    await control.setValue(true)
    await waitDebounce()
    expect(wrapper.vm.hasChanged).toEqual(true)
  })
})
