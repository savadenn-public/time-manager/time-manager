# Time Manager App

Access full documentation :

* [Stable online documentation](https://savadenn-public.gitlab.io/time-manager/time-manager/docs/)
* [Documentation source](./docs)
* Build `dist/docs/TimeManager.pdf` locally
  ```
  make build.doc
  ```

## Contact

You may join us on our public [Matrix room](https://matrix.to/#/#time-manager:savadenn.ems.host)

## Authors and contributors

Time Manager is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found [here](https://gitlab.com/savadenn-public/time-manager/time-manager/-/graphs/stable).
