<template>
  <component
    :is="tag"
    :class="{ 'EntryBlock--isRunning': !entry.stop }"
    class="EntryBlock"
    v-if="entry"
  >
    <AutoComplete
      v-model="entry.context"
      :placeholder="t('entry.context')"
      :suggestions="contextSuggestions"
      class="EntryBlock__Context"
      @complete="getContexts"
    />

    <Inplace v-model:active="contentActive" class="EntryBlock__Content">
      <template #display>
        <Editor
          :modelValue="firstLine || t('entry.content')"
          class="EntryBlock__ContentLess"
          readonly
        />
      </template>
      <template #content>
        <div class="EntryBlock__ContentMore">
          <Editor v-model="entry.content">
            <template #toolbar>
              <span class="ql-formats">
                <Button
                  :icon="Icons.less"
                  class="Button--outlined Button--noBorder Color--primary"
                  @click="closeContent"
                />
              </span>
              <span class="ql-formats">
                <button class="ql-bold" type="button"></button>
                <button class="ql-italic" type="button"></button>
                <button class="ql-underline" type="button"></button>
              </span>
              <span class="ql-formats">
                <button class="ql-list" type="button" value="ordered"></button>
                <button class="ql-list" type="button" value="bullet"></button>
              </span>
              <span class="ql-formats">
                <button class="ql-link" type="button"></button>
                <button class="ql-code-block" type="button"></button>
              </span>
              <span class="ql-formats">
                <button class="ql-clean" type="button"></button>
              </span>
            </template>
          </Editor>
        </div>
      </template>
    </Inplace>

    <StopWatch
      :now="entry.stop || Now"
      :start="entry.start"
      class="EntryBlock__Duration"
    />

    <SplitDateTimePicker
      v-if="entry.stop"
      v-model="entry.stop"
      :isDark="isDark"
      :locale="locale"
      :timeZone="timeZone"
      class="EntryBlock__Stop"
      fold-date
    />

    <SplitDateTimePicker
      v-model="entry.start"
      :isDark="isDark"
      :locale="locale"
      :timeZone="timeZone"
      class="EntryBlock__Start"
      fold-date
    />

    <Button
      v-if="!entry.stop"
      :icon="Icons.entry.stop"
      :title="t('entry.stop')"
      class="
        EntryBlock__Action
        Button--outlined Button--noBorder
        Color--primary
      "
      @click="stop"
    />

    <Button
      v-if="entry.stop"
      :icon="Icons.remove"
      :title="t('entry.remove')"
      class="EntryBlock__Action Button--outlined Button--noBorder Color--danger"
      @click="remove"
    />
  </component>
</template>

<script lang="ts">
import {
  computed,
  defineComponent,
  onMounted,
  onUnmounted,
  reactive,
  ref,
  toRefs,
  watch,
} from 'vue'
import {
  Button,
  Icon,
  InputField,
  SplitDateTimePicker,
  StopWatch,
} from '@/components'
import * as Types from '@/types'
import { Icons } from '@/Icons'
import { locale, Now, theme, timeZone } from '@/states'
import { useTranslation } from '@/components/UseTranslation'
import { Entries } from '@/stores'
import moment from 'moment-timezone'

export default defineComponent({
  name: 'EntryBlock',
  emits: ['groups', 'removed'],
  components: { Button, Icon, StopWatch, InputField, SplitDateTimePicker },
  props: {
    id: {
      type: String,
      required: true,
    },
    tag: {
      type: String,
      required: false,
      default: 'div',
    },
    tabindex: {
      type: Number,
      required: false,
      default: 0,
    },
  },

  setup(props, context) {
    const dbData = reactive<{ entry: Types.Entry | undefined }>({
      entry: undefined,
    })

    // Set up reactivity using observer pattern
    const observer = Entries.Observer.createObjectObserver({
      targetId: props.id,
      methods: {
        onPut: (event) => {
          dbData.entry = event.payload
        },
      },
    })
    observer.start()
    onUnmounted(() => observer.stop())

    /**
     * Uses teleport Vue feature to place entry in correct parent
     */
    const groups = computed((): Types.GroupEntries[] => {
      if (dbData.entry) {
        if (!dbData.entry.stop) {
          return [{ selector: Types.GroupEntriesBaseClass + 'Running' }]
        } else {
          return [{ selector: Types.GroupEntriesBaseClass + 'Stopped' }]
        }
      }
      return []
    })

    watch(
      () => dbData.entry,
      (entry: Types.Entry | undefined) => {
        if (entry) Entries.put(entry)
      },
      { deep: true }
    )

    const contentActive = ref<boolean>(false)

    onMounted(async () => {
      dbData.entry = await Entries.get(props.id)
      if (!dbData.entry) context.emit('removed')
    })

    const contextSuggestions = ref<string[]>([])

    return {
      ...toRefs(dbData),
      Now,
      locale,
      timeZone,
      Icons,
      firstLine: computed(() => dbData.entry?.content.split('\n')[0]),
      isDark: computed(() => theme.value.includes('Dark')),
      /**
       * Uses teleport Vue feature to place entry in correct parent
       */
      targetSelectors: computed(() =>
        groups.value.map((group) => group.selector)
      ),
      /**
       * Place of this entryBlock among his sibling, using order CSS (reverse for latest at top)
       */
      order: computed(() =>
        dbData.entry ? -moment(dbData.entry.start).unix() : 0
      ),
      ...useTranslation(),
      stop: () => {
        if (dbData.entry) Entries.stop(dbData.entry)
      },
      remove: async () => await Entries.remove(props.id),
      contentActive,
      closeContent: () => {
        contentActive.value = false
      },
      contextSuggestions,
      getContexts: async (event: { query: string }) => {
        const contexts = await Entries.getAllContexts()
        contextSuggestions.value = contexts.filter(
          (context) =>
            context.toLowerCase().includes(event.query.toLowerCase()) &&
            context !== event.query
        )
      },
    }
  },
})
</script>

<style lang="scss" scoped>
@use '../../style/variables' as v;

.EntryBlock {
  display: inline-grid;
  grid-template-areas:
    'context duration stop  action'
    'context duration start action'
    'content content content content';
  grid-template-rows: repeat(3, minmax(min-content, auto));
  grid-template-columns:
    1fr
    minmax(70px, auto)
    repeat(2, minmax(min-content, auto));
  grid-row-gap: 0;
  grid-column-gap: v.$spacing;
  align-items: center;
  justify-content: center;
  padding: v.$spacing-2;

  @media screen and (max-width: v.$minWidth_tablet) {
    grid-template-areas:
      'duration stop  action'
      'duration start action'
      'context context context'
      'content content content';
    grid-template-rows: repeat(4, minmax(min-content, auto));
    grid-template-columns:
      1fr
      repeat(2, minmax(min-content, auto));
  }

  &--isRunning {
    grid-template-areas:
      'context duration start action'
      'content content content content';

    @media screen and (max-width: v.$minWidth_tablet) {
      grid-template-areas:
        'duration start action'
        'context context context'
        'content content content';
    }
  }

  &__Context {
    grid-area: context;
    border: none;
    background-color: transparent;

    //noinspection CssInvalidPseudoSelector
    :deep(.p-autocomplete-input) {
      width: 100%;
      border: none;
      background: transparent;
    }
  }

  &__Content {
    grid-area: content;
  }

  &__ContentLess {
    overflow-y: hidden;
    max-height: 30px;

    //noinspection CssInvalidPseudoSelector
    :deep(.p-editor-toolbar) {
      display: none;
    }

    //noinspection CssInvalidPseudoSelector
    :deep(.p-editor-content.ql-container),
    :deep(.p-editor-content.ql-container .ql-editor) {
      padding-top: 0;
      border: none;
      background: transparent;
    }
  }

  &__ContentMore {
    position: relative;
  }

  &__ContentReduce {
    position: absolute;
    top: v.$spacing;
    right: v.$spacing;
    z-index: 10;
  }

  &__Duration {
    display: inline-flex;
    grid-area: duration;
    align-items: flex-start;
    justify-content: flex-start;
    font-size: 0.8rem;
    white-space: pre;
  }

  &__Start,
  &__Stop {
    align-items: center;
    justify-content: flex-end;
    font-size: 0.8rem;
  }

  &__Start {
    grid-area: start;
  }

  &__Stop {
    grid-area: stop;
  }

  &__Action {
    grid-area: action;
    height: 100%;
  }
}
</style>
