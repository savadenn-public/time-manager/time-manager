import * as Types from '@/types'
import { reactive } from 'vue'

export const pwaUpdate: Types.States.PwaUpdate = reactive({
  refreshing: false,
  registration: null,
  updateExists: false,
  read() {
    this.updateExists = false
  },
})

// Reload app when ServiceWorker updates
if (navigator.serviceWorker) {
  navigator.serviceWorker.addEventListener('controllerchange', () => {
    if (pwaUpdate.refreshing) return
    pwaUpdate.refreshing = true
    window.location.reload()
  })
}
