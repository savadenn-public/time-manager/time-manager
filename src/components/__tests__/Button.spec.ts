import { shallowMount } from '@vue/test-utils'
import { Button } from '..'

describe('Components/Button.vue', () => {
  it('renders label when passed', async () => {
    const label = 'my button'
    const wrapper = shallowMount(Button, { props: { label } })
    expect(wrapper.text()).toMatch(label)
  })

  it('triggers action when clicked', async () => {
    const action = jest.fn()
    const wrapper = shallowMount(Button, {
      props: { label: '' },
      attrs: { onclick: action },
    })
    await wrapper.trigger('click')
    expect(action.mock.calls.length).toEqual(1)
  })
})
