/**
 * A time Entry
 */
export interface Entry {
  /**
   * Duration of the entry in milliseconds
   */
  duration: number

  /**
   * Id of the entry
   */
  id: string

  /**
   * Start date
   */
  start: Date

  /**
   * Stop date
   */
  stop?: Date

  /**
   * Content
   */
  content: string

  /**
   * Context
   */
  context?: string
}

/**
 * Sort function for entry
 * @param e1
 * @param e2
 */
export function sortEntry(e1: Entry, e2: Entry) {
  return e2.start.getTime() - e1.start.getTime()
}
