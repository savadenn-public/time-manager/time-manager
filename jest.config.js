module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.tsx?$': 'ts-jest',
  },
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  collectCoverageFrom: [
    'src/**/*.{js,ts,vue}',
    '!src/types/**',
    '!src/**/__tests__/**',
  ],
  reporters: ['jest-junit', 'jest-html-reporters'],
  coverageReporters: ['lcov', 'cobertura', 'text-summary'],
}
