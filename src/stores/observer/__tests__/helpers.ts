import * as Types from '@/types'
import { FactoryObservers } from '@/stores/observer'

export type testObject = { id: string }
export const testId = 'testId'

/**
 * Returns mocks for StoreObserver method
 */
export function getMockedStoreObserverMethods(): Types.StoreObserverMethods<testObject> {
  return {
    onDelete: jest.fn(),
    onPut: jest.fn(),
  }
}

/**
 * Return object for testing
 */
export function getTestingSample() {
  const observerFactory = new FactoryObservers<testObject>()

  const storeObserverMethods = getMockedStoreObserverMethods()
  const objectObserverMethods = getMockedStoreObserverMethods()
  return {
    factory: observerFactory,
    storeObserverMethods,
    storeObserver: observerFactory.createStoreObserver({
      methods: storeObserverMethods,
    }),
    objectObserverMethods,
    objectObserver: observerFactory.createObjectObserver({
      targetId: testId,
      methods: objectObserverMethods,
    }),
  }
}

export type TestingSample = ReturnType<typeof getTestingSample>

/**
 * Tests that observers are idle
 * @param params
 */
export function expectMethodsCallTimes(params: {
  sample: TestingSample
  expected: {
    store: { onPut: number; onDelete: number }
    object: { onPut: number; onDelete: number }
  }
}) {
  expect(params.sample.storeObserverMethods.onPut).toHaveBeenCalledTimes(
    params.expected.store.onPut
  )
  expect(params.sample.storeObserverMethods.onDelete).toHaveBeenCalledTimes(
    params.expected.store.onDelete
  )
  expect(params.sample.objectObserverMethods.onPut).toHaveBeenCalledTimes(
    params.expected.object.onPut
  )
  expect(params.sample.objectObserverMethods.onDelete).toHaveBeenCalledTimes(
    params.expected.object.onDelete
  )
}
