import { shallowMount } from '@vue/test-utils'
import { Link } from '..'
import { COMPONENT_LINK_ERROR_MULTIMODE } from '@/types'

describe('Components/Link.vue', () => {
  it('renders href link', async () => {
    const href = 'https://target.exemple/'
    const title = 'My title'
    const slotContent = 'my slot string'
    const wrapper = shallowMount(Link, {
      global: {
        stubs: {
          routerLink: true,
        },
      },
      props: { href, title },
      slots: {
        default: slotContent,
      },
    })
    const link = wrapper.find('a')
    expect(link.exists()).toBe(true)
    expect(link.element.title).toEqual(title)
    expect(link.element.href).toEqual(href)
    expect(link.text()).toMatch(slotContent)
  })

  it('throw error if to and href props are set', async () => {
    const href = 'https://target.exemple/'
    const to = { name: 'home' }
    expect(() => shallowMount(Link, { props: { href, to } })).toThrow(
      COMPONENT_LINK_ERROR_MULTIMODE
    )
  })
})
