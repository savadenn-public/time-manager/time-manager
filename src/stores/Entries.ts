import * as Types from '@/types'
import { StoreAction } from '@/types'
import * as DB from './DB'
import { isReactive, toRaw, unref } from 'vue'
import moment from 'moment-timezone'
import { Entries } from '@/stores/index'
import { Now, settings } from '@/states'
import { v4 as uuid4 } from 'uuid'
import { FactoryObservers } from '@/stores/observer'

export const Observer = new FactoryObservers<Types.Entry>()

/**
 * Read only access to the store
 */
export async function storeRead() {
  return (await DB.promise).transaction('entries', 'readonly').store
}

/**
 * Write access to the store
 */
async function storeWrite() {
  return (await DB.promise).transaction('entries', 'readwrite').store
}

/**
 * Gets entry by id
 */
export async function get(id: string) {
  return (await storeRead()).get(id)
}

/**
 * Saves current entry
 *
 * @param entry
 */
export async function put(entry: Types.Entry) {
  const standardEntry = isReactive(entry) ? toRaw(entry) : unref(entry)
  const result = (await storeWrite()).put(standardEntry)
  Observer.trigger({ action: StoreAction.PUT, payload: entry })
  return result
}

/**
 * Removes entry by id
 * @param id
 */
export async function remove(id: string) {
  const result = (await storeWrite()).delete(id)
  Observer.trigger({ id, action: StoreAction.DELETE })
  return result
}

/**
 * Retrieves all entries
 */
export async function all() {
  return (await storeRead()).index('by-start').getAll()
}

/**
 * Stops given entry
 * @param entry
 */
export async function stop(entry: Types.Entry) {
  if (!entry) return
  entry.stop = Now.value.startOf('second').toDate()
  entry.duration = moment(entry.stop).diff(entry.start)
  await Entries.put(entry)
  return entry
}

/**
 * Stop all entries
 */
export async function stopAll() {
  const store = await Entries.storeRead()
  for (const entry of await store.index('by-duration').getAll(-1)) {
    if (!entry.stop) await stop(entry)
  }
}

/**
 * Start a new entry
 */
export async function start() {
  if (settings.stopOnStart) await stopAll()
  const newEntry: Types.Entry = {
    id: uuid4(),
    content: '',
    start: Now.value.startOf('second').toDate(),
    duration: -1,
  }
  await put(newEntry)
}

/**
 * Get all contexts
 */
export async function getAllContexts() {
  const store = await Entries.storeRead()
  const contexts: string[] = []
  for await (const cursor of store
    .index('by-context')
    .iterate(undefined, 'nextunique')) {
    if (cursor.value.context) {
      contexts.push(cursor.value.context)
    }
  }
  return contexts
}

/**
 * Normalize all entries
 */
export async function normalizeAll() {
  const store = await storeWrite()
  const promises = []
  for await (const cursor of store.iterate(undefined)) {
    if (cursor.value) {
      cursor.value.duration = moment(cursor.value.stop).diff(cursor.value.start)
      promises.push(Entries.put(cursor.value))
    }
  }
  return Promise.all(promises)
}
