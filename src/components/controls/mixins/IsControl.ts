import { hasUuid, hasChanged, HandlesModelParams, HasChangedParams } from '.'

import { useTranslation } from '@/components/UseTranslation'
import { Icons } from '@/Icons'
import {
  handlesModel,
  ModelEmits,
} from '@/components/controls/mixins/HandlesModel'

export const ControlEmits = [...ModelEmits]

export type ControlParams<ValueType> = HandlesModelParams<ValueType> &
  HasChangedParams<ValueType>

/**
 * Creates an object to use in setup of a control
 *
 * @param params
 * @constructor
 */
export function isControl<ValueType>(params: ControlParams<ValueType>) {
  return {
    ...handlesModel<ValueType>(params),
    ...useTranslation(),
    ...hasChanged<ValueType>(params),
    ...hasUuid(),
    Icons,
  }
}
