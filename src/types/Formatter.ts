import { Entry } from './internal'

/**
 * A formatter interface
 */
export interface Formatter {
  contentType: string
  filename: string

  /**
   * Format all given entries into a string
   * @param entries
   */
  format(entries: Partial<Entry>[]): string
}
