import { Formatter } from './internal'
import { Entry } from '@/types'
import stringify from 'csv-stringify/lib/sync'

/**
 * Columns Order for CSV export
 */
const CSV_ORDER: Array<keyof Entry> = [
  'id',
  'start',
  'stop',
  'duration',
  'context',
  'content',
]

/**
 * CSV Separator
 */
const CSV_SEPARATOR = ';'

export class CsvFormatter extends Formatter {
  contentType = 'text/csv;charset=utf-8'
  filename = 'entries.csv'

  formatSpecific(entries: Partial<Entry>[]): string {
    return stringify(entries, {
      delimiter: CSV_SEPARATOR,
      columns: CSV_ORDER,
      header: true,
      cast: { date: (value) => value.toISOString() },
    })
  }
}

/**
 * Singleton instance of CsvFormatter
 */
export const csvFormatter = new CsvFormatter()
