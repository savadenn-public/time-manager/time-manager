import Settings from './Settings.vue'
import Home from './Home.vue'
import NavBar from './partial/NavBar.vue'
import Footer from './partial/Footer.vue'
import Entry from './partial/EntryBlock.vue'

export { Settings, Home, NavBar, Entry, Footer }
