import { computed } from 'vue'
import { ModelProps } from '.'

export interface HasChangedProps<ValueType> extends ModelProps<ValueType> {
  restoreValue?: ValueType | null
}

export interface HasChangedParams<ValueType> {
  props: HasChangedProps<ValueType>
}

/**
 * Returns object to spread in setup of a component that can show changes
 * @param params
 */
export function hasChanged<ValueType>(params: HasChangedParams<ValueType>) {
  return {
    hasChanged: computed(
      () =>
        params.props.restoreValue !== undefined &&
        params.props.restoreValue !== params.props.modelValue
    ),
  }
}
